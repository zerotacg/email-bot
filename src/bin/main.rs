use std::env;

use email_bot::DEFAULT_INBOX_FOLDER;

fn main() {
    let args: Vec<String> = env::args().collect();
    let default_inbox_folder = String::from(DEFAULT_INBOX_FOLDER);
    let domain = args.get(1).unwrap();
    let username = args.get(2).unwrap();
    let password = args.get(3).unwrap();
    let inbox_folder = args.get(4).unwrap_or(&default_inbox_folder);
    let body = fetch_mailbox_top(domain, username, password, inbox_folder).expect("inbox should contain at least one message").unwrap_or_default();
    println!("email body: {:?}", body);
}

extern crate imap;
extern crate native_tls;

fn fetch_mailbox_top(domain: &str, username: &String, password: &String, mailbox_name: &String) -> imap::error::Result<Option<String>> {
    let tls = native_tls::TlsConnector::builder().build().unwrap();

    // we pass in the domain twice to check that the server's TLS
    // certificate is valid for the domain we're connecting to.
    let client = imap::connect((domain, 993), domain, &tls).unwrap();

    // the client we have here is unauthenticated.
    // to do anything useful with the e-mails, we need to log in
    let mut imap_session = client
        .login(username, password)
        .map_err(|e| e.0)?;

    // we want to fetch the first email in the INBOX mailbox
    imap_session.select(mailbox_name)?;

    let query = "(BODY[HEADER.FIELDS (SUBJECT)] BODY[TEXT])";
    let messages = imap_session.fetch("1", query)?;
    let message = if let Some(m) = messages.iter().next() {
        m
    } else {
        return Ok(None);
    };

    // extract the message's body
    let body = message.text().expect("message did not have a body!");
    let body = std::str::from_utf8(body)
        .expect("message was not valid utf-8")
        .to_string();
    let header = message.header().expect("message did not have a header!");
    let header = std::str::from_utf8(header)
        .expect("header was not valid utf-8")
        .to_string();
    let subject = header.strip_prefix("Subject: ").expect("message should have a subject");
    println!("email subject: {:?}", subject);

    // be nice to the server and log out
    imap_session.logout()?;

    Ok(Some(body))
}
